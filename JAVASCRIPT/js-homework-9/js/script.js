// Задание
// Реализовать переключение вкладок (табы) на чистом Javascript.
//
//     Технические требования:
//
//     В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
//     Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
//     Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.


let tabsTitle, tabsContent, tabs;

window.onload = function () {
    tabsTitle = document.getElementsByClassName("tabs-title");
    tabsContent = document.getElementsByClassName("tabs-content");
    hideTabsContent(1);
};

function hideTabsContent(a) {
    for (let i = a; i<tabsContent.length; i++){
        tabsContent[i].classList.remove("show");
        tabsContent[i].classList.add("hide");
        tabsTitle[i].classList.remove("active");
    }
}

tabs = document.getElementById("tabs");

tabs.onclick = function (event) {
    let target = event.target;
    if (target.className !== "tabsTitle"){
        for (let i=0; i < tabsTitle.length; i++){
            if (target === tabsTitle[i]){

                if (tabsContent[i].classList.contains("hide")){
                    hideTabsContent(0);
                    tabsTitle[i].classList.add("active");
                    tabsContent[i].classList.remove("hide");
                    tabsContent[i].classList.add("show");
                } break;
            }
        }
    }
};