// Теоретический вопрос:
//
// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
//DOM - це представлення вигляду сторінки браузера у вигляді обєктів які можна змінювати. Або у вигляді дерева тегів.
// DOM - дає можливість динамічно взаємодіяти із змінами на сторінці
// Задание
// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка.
//
//     Технические требования:
//     Создать функцию, которая будет принимать на вход массив.
//     Каждый из элементов массива вывести на страницу в виде пункта списка
// Необходимо использовать шаблонные строки и функцию map массива для формирования контента списка перед выведением его на страницу.
//     Примеры массивов, которые можно выводить на экран:
//     ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
//         ['1', '2', '3', 'sea', 'user', 23]
// Можно взять любой другой массив.
//
//     Не обязательное задание продвинутой сложности:
//     ??? Очистить страницу через 10 секунд. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.
//     Если внутри массива одним из элементов будет еще один массив или объект, выводить его как вложенный список.

function createList(fArray) {

   let listItem = (fArray.map(function (num) {
        return (`<li><a href="#">${num}</a></li>`);
    })).join("");

   let list = (`<ul>${listItem}</ul>`);

    let listBlock = document.createElement("div");
    document.body.appendChild(listBlock);
    listBlock.className = "myDiv";
    listBlock.innerHTML = list;

    let timer = document.createElement("div");
    document.body.appendChild(timer);
    timer.className = "myTimer";

    let counter = 10;
    timer.innerHTML = `Привет! Осталось: 10 сек.`;
    setInterval(function() {
        if(counter > 0) { counter--;
            timer.innerHTML = `Привет! Осталось: ${counter} сек.`;
        } else {listBlock.remove(); timer.remove();}
    }, 1000);


}
let tArray = ['1', '2', '3', 'sea', 'user', 23];
let fArray = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv', tArray];
createList(fArray);