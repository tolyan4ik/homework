// Теоретический вопрос
//
// Опишите своими словами, что такое метод обьекта
//
//
// Задание
// Реализовать функцию для создания объекта "пользователь".
//
//     Технические требования:
//
//     Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
//     При вызове функция должна спросить у вызывающего имя и фамилию.
//     Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
//     Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.
//
//
//     Не обязательное задание продвинутой сложности:
//
//     Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.

    function  createNewUser (){
    let firstName = prompt("please write your first name", "Петро");
    let lastName = prompt("please write your last name", "Відун");
    let newUser = {
        name: firstName,
        surname: lastName,
        getLogin: function () {
            return (this.name.charAt(0) + this.surname).toLowerCase();
        }
    };
    return newUser;
}
let person = createNewUser();
let login = person.getLogin();
console.log(login);