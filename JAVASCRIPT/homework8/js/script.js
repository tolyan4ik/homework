//Опишите своими словами, как Вы понимаете, что такое обработчик событий.
// обработчик события, тоесть функиция которая сработает на событие.события это те действия которые происходят в момент нажатия, наведения , или то каких-то других функций на какой то определенный обьект
//мы навели на обьект сработала функция, нажали сработала функция)) 2 раза нажили и тд

// Технические требования:
//
//     При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
//     При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
//     Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
//     При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
//     Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
//
//
//     В папке img лежат примеры реализации поля ввода и создающегося span.

let input = document.getElementById("inputPrice"),
    block = document.getElementsByClassName("priceBlock")[0],
    clearPrice = document.createElement("button"),
    span = document.createElement('span'),
    redPrice = document.createElement("p"),
    inputValue = "";

input.onfocus = function () {
    input.style.borderColor = "green";
    input.style.color = "";
    input.style.borderRadius = "8px";
    inputValue = "";
};

input.onblur = function () {
    input.style.borderColor = "";
    input.style.color = "green";
    input.style.borderRadius = "8px";
    inputValue = input.value;


    clearPrice.innerHTML = "x";
    clearPrice.id = "clearPrice";

    span.innerHTML = " Текущая цена: $" + inputValue + " ";
    span.id = "spanPrice";

    if (inputValue > 0) {
        if (!(document.getElementById("spanPrice"))) {
            document.createElement("span");
            span.id = "spanPrice";
            block.insertBefore(span, input);
            block.insertBefore(clearPrice, input);
            if ((document.getElementById("redPrice"))) {
                redPrice.remove();
            }
        }
    } else {
        if (!(document.getElementById("redPrice"))) {
            redPrice.id = "redPrice";
            redPrice.innerHTML = "Please enter correct price";
            block.appendChild(redPrice);
            input.style.borderColor = "red";
        }
    }
};


clearPrice.onclick = function () {
    redPrice.remove();
    span.remove();
    clearPrice.remove();
    inputValue = "";
    input.value = "";
};