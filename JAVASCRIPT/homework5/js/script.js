// Задание
// // // // Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
// // // //
// // // //     Технические требования:
// // // //
// // // //     Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
// // // //
// // // //     При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
// // // //     Создать метод getAge() который будет возвращать сколько пользователю лет.
// // // //     Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
// // // //
// // // //
// // // // Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.


function  createNewUser (){
    let firstName = prompt("please write your first name", "Петро");
    let lastName = prompt("please write your last name", "Відун");

    let birthdaySplitted = [];
    let hasError = false;

    do {
        let birthday = prompt(`${hasError ? 'Неверный формат или дата некорректна. ' : ''}Введите дату в формате dd.mm.yyyy`, "12.08.1989");
        birthdaySplitted = birthday.split('.').map(x => {
            while (x.startsWith('0')){
                x = x.slice(1);
            }

            return parseInt(x);
        });

        hasError = birthdaySplitted.length !== 3 || //not 3 parts separated by dots
            birthdaySplitted.some(part => isNaN(part)) || //some part is NaN
            birthdaySplitted[2] < 100 || //incorrect year value
            birthdaySplitted[1] < 1 || birthdaySplitted[1] > 12 || //incorrect month value
            birthdaySplitted[0] < 1 || birthdaySplitted[0] > 31; //incorrect day value
    } while(hasError);

    let newUser = {
        name: firstName,
        surname: lastName,
        birthday: new Date(birthdaySplitted[2],birthdaySplitted[1] - 1,birthdaySplitted[0]),
        getLogin: function () {
            return (this.name.charAt(0) + this.surname).toLowerCase();
        },

        getAge: function() {
            let now = new Date();
            let baseDate = new Date(this.birthday.getFullYear(), this.birthday.getMonth(), this.birthday.getDay());
            let years = -1;

            while (baseDate <= now){
                years++;
                baseDate.setFullYear(baseDate.getFullYear() + 1);
            }
            return years;
        }
    };
    return newUser;
}


let person = createNewUser();
let login = person.getLogin();
let age = person.getAge();
let getPassword = (login + person.birthday.getFullYear());

console.log(person.birthday);
console.log(login);
console.log(age);
console.log(getPassword);