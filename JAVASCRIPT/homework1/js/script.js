// Реализовать простую программу на Javascript, которая будет взаимодействовать с пользователем с помощью модальных окон браузера - alert, prompt, confirm.
// //
// //     Технические требования:
// //
// //     Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
// //     Если возраст меньше 18 лет - показать на экране сообщение: You are not allowed to visit this website.
// //     Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением: Are you sure you want to continue? и кнопками Ok, Cancel. Если пользователь нажал Ok, показать на экране сообщение: Welcome, + имя пользователя. Если пользователь нажал Cancel, показать на экране сообщение: You are not allowed to visit this website.
// //     Если возраст больше 22 лет - показать на экране сообщение: Welcome, + имя пользователя.
// //     Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.
// //
// //
// //     Не обязательное задание продвинутой сложности:
// //
// //     После ввода данных добавить проверку их корректности. Если пользователь не ввел имя, либо при вводе возраста указал не число - спросить имя и возраст заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).

let fName = "";

do {
    fName = prompt("Enter your name:", fName );
} while (parseInt(fName) || parseInt(fName) === 0 || fName === "");

let age;

do {
    age = +prompt('Enter your age please:', age);
} while(isNaN(age) || age <= 0);

if (age < 18) {
    alert ('You are not allowed to visit this website');
} else if (age <= 22){
    let agree = confirm('Are you sure you want to continue?')
    if (agree){
        alert (' Welcome,'+ fName);
    } else {
        alert ("You are not allowed to visit this website!")
    }
} else {
    confirm('Are you sure you want to continue?')
}

