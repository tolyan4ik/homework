// Теоретический вопрос
//
// Опишите своими словами как работает цикл forEach.
//
//
//     Задание
// Реализовать функцию фильтра массива по указанному типу данных.
//
//     Технические требования:
//
//     Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
//     Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом. То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string', то функция вернет массив [23, null].

// Метод forEach()використовується для перебора кожного елемента в масиві, як більш красивий в порівнняні з for...
//     У нього є такі параметри:
//     callback - Функція, яка буде визвана для кожного елемента масива; у неї буде передано три аргумента:
// - currentValue ( теперішній елемент масива)
// - index ( необов'язковий  ) -індекс теперішнього  елемента який оброблюється в масиві.
// - array(необов'язковий )- масив по якому буде йти прохід
//   thisArg ( необов'язковий  параметр) це  значенння, яке використовується в якості this при визові функції callback.)

// приклади:
// только с callback функцией
// array.forEach( function( currentValue, index, arr ) );
// с использованием объекта, на который может ссылаться ключевое слово this
// array.forEach( function( currentValue, index, arr ), thisValue );

function filterBy (firstArray, type){

    firstArray.forEach(function(item) {
        if (typeof item !== type) secondArray.push(item)
    })
}
//додатково в кінці дописано true для тесту
let firstArray = ['hello', 'world', 23, '23', null, true], type = 'string';
let secondArray = [];

filterBy (firstArray, type);
console.log(secondArray);

