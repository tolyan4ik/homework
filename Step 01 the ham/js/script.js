//-------------tabs our services-------
$('.service-tab').click(function () {
    $('.service-tab').removeClass('tab-active');
    $(this).addClass('tab-active');
    $('.tabs-box').addClass('tabs-box-none');
    $('.tabs-box[rel='+$(this).attr('rel')+']').removeClass('tabs-box-none')
});

$('.button-load-more').click(function (e) {
    $('.show-more-image').addClass('active');
    e.preventDefault();
});

//---------------list item  --------------
$('.list-item').click(function (event) {
    $('.list-item').removeClass('list-item-active');
    $(this).addClass('list-item-active');

    x = $(this).data('menu');
    console.log(x);
    $('.image-gallery').removeClass('active-image');
    y = $(`.image-gallery[data-menu*="${x}"]`).addClass('active-image');

    event.preventDefault();
    console.log(y);
});


// Masonry--------------------------------------------\

function masonryF(){
    $(document).ready(function(){
        let grid = document.querySelector('.block-08-gallery-images');
        let pckry = new Packery( grid, {
            itemSelector: '.block-08-gallery-image'
        });

    });
}
masonryF();

// interval--------------------------------------------\

function interval(intervalDivName) {
    $(intervalDivName).removeClass("hide");
    setTimeout(function(){$(intervalDivName).addClass("hide")},2000);
}

// loadingImg-------------------------------------------\

$(document).ready(function(){
    $(".block-05-ourwork-image-hide").each(function () {$(this).hide();});
    $(".block-05-ourwork-image-hide-2").each(function () {$(this).hide();});
});


function loadImgButton(nameIdButton, nameHide1, nameHide2, nameHide3, intervalDivName, foo){
    let $count = 0;
    $(nameIdButton).click(function () {
        if ($count === 0) {
            interval(intervalDivName);
            setTimeout(function(){
                $(`.${nameHide1}`).fadeIn(1000, "linear").show();
                $(`.${nameHide1}`).removeClass(nameHide1);
                $(`.${nameHide1}`).removeClass(nameHide3);
                foo();
            },2000);
            $count++
        } else if ($count === 1) {
            interval(intervalDivName);
            setTimeout(function(){
                $(`.${nameHide2}`).fadeIn(1000, "linear").show();
                $(`.${nameHide2}`).removeClass(nameHide2);
                $(`.${nameHide1}`).removeClass(nameHide3);
                $(nameIdButton).addClass("hide");
                foo();

            },2000);

        }
    });
}
loadImgButton();
loadImgButton("#block-05-ourwork-button","block-05-ourwork-image-hide",
    "block-05-ourwork-image-hide-2", "", "#ourwork-image-load", clickSecondTab);
loadImgButton("#block-08-gallery-button", "block-08-gallery-image-hide",
    "block-08-gallery-image-hide-2", "hide", "#ourwork-image-load-2", masonryF);

//  Block 5 -Our Amazing Work-------------------------------------------

let $tabsTitle, $tabsContent;

$tabsTitle = $(".block-05-ourwork-divtab");
$tabsContent = $(".block-05-ourwork-image");

function tabsContSecTab(target){
    $(".block-05-ourwork-image").each(function () {
        if  ($(this).hasClass("block-05-ourwork-image-hide")
            || $(this).hasClass("block-05-ourwork-image-hide-2"))
            $(this).hide();

        else{
            if ($(target).index() === 0) $(this).show();
            if($(target).index() > 0) {
                if ($(this).hasClass(`block-05-ourwork-image-list-${$(target).index()}`))
                    $(this).show();
            }
        }

    })
}
function hideTabsContentSecondTab() {
    $tabsContent.each(function () {
        if ($(this).index() >= 0) $(this).hide();});
    $tabsTitle.each(function () {
        if ($(this).index() >= 0) $(this).removeClass("block-05-ourwork-divtab-active");});
}


$(".block-05-ourwork-divtabs").click(function (event) {
    let target = event.target;

    hideTabsContentSecondTab();
    $(target).addClass("block-05-ourwork-divtab-active");
    tabsContSecTab(target);
});

function clickSecondTab(){
    let target = $(".block-05-ourwork-divtab-active")[0];
    $(".block-05-ourwork-image").each(function () {
        if ($(this).index() >= 0) $(this).hide();});
    tabsContSecTab(target);
}

// block-07-people--------------------------------------------
$(document).ready(function(){
    $('.block-07-people-cards').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.block-07-people-minicards'
    });
    $('.block-07-people-minicards').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        adaptiveHeight: true,
        asNavFor: '.block-07-people-cards',
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        centerPadding: false

    });

});